(lazy-major-mode "\\.php$" php-mode)

(after 'php-mode
  (require 'php-doc)

  
  (add-hook 'php-mode-hook (lambda () 
                             (flycheck-select-checker 'php)
;;;                           (setq c-default-style "psr2") ;Does not work
                             (c-set-style "psr2") ; Works
;;;                           (setq php-mode-coding-style 'PSR-2) ;Does not work
                             ))
  )

(provide 'init-php)
