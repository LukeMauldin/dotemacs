(setq helm-command-prefix-key "C-c h")
(setq helm-quick-update t)
(setq helm-bookmark-show-location t)
(setq helm-buffers-fuzzy-matching t)

(helm-mode 1)

(require-package 'helm)
(require-package 'helm-swoop)

(after 'projectile
  (require-package 'helm-projectile))

;;;Replace Emacs find-file and buffer commands
(global-set-key (kbd "C-x C-f") 'helm-find-files)
;(global-set-key (kbd "C-x b") 'helm-buffers-list)

(require 'helm-config)

(provide 'init-helm)
